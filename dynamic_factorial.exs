// factorial tail recursive (Elixir style)

// 4! means 1*2*3*4, we can ignore 1, which makes it 2*3*4 = 24

def factorial(num), do: factorial(num, 1)
defp factorial(num, _product) when num < 0, do: raise ArithmeticError
defp factorial(num, product) when num <= 1, do: product
defp factorial(num, product), do: factorial(num - 1, product * num)